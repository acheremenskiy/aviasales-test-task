export interface ITicket {
  // Цена в рублях
  price: number
  // Код авиакомпании (iata)
  carrier: string
  // Массив перелётов.
  // В тестовом задании это всегда поиск "туда-обратно" значит состоит из двух элементов
  segments: [
    {
      // Код города (iata)
      origin: string
      // Код города (iata)
      destination: string
      // Дата и время вылета туда
      date: string
      // Массив кодов (iata) городов с пересадками
      stops: string[]
      // Общее время перелёта в минутах
      duration: number
    },
    {
      // Код города (iata)
      origin: string
      // Код города (iata)
      destination: string
      // Дата и время вылета обратно
      date: string
      // Массив кодов (iata) городов с пересадками
      stops: string[]
      // Общее время перелёта в минутах
      duration: number
    }
  ]
}

export const getSearchId = async () => fetch('https://front-test.beta.aviasales.ru/search', { method: 'GET' })
  .then(response => response.json());

const fetchTickets = async (searchId: string): Promise<any> => {
  return await fetch('https://front-test.beta.aviasales.ru/tickets?searchId=' + searchId, { method: 'GET' })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw Error(`${response.status}`);
      }
    })
    .catch(error => {
      if (error.message === '500') {
        console.warn('Искусственная ошибка сервера, для усложнения задачи');
        return fetchTickets(searchId);
      } else {
        console.warn('Неизвестная ошибка');
      }
    });
};

export const getTickets = async (searchId: string) => {
  let stop = false;
  const ticketsArray = [];

  do {
    const ticketsChunk = await fetchTickets(searchId);
    ticketsArray.push(...ticketsChunk.tickets);
    stop = ticketsChunk.stop;
  } while (!stop);

  return ticketsArray;
};
