import React from 'react';
import styled from 'styled-components';
import Styles from '../../styles';

interface ITabs {
  activeTab: string,
  setActiveTab: (activeTab: string) => void,
}

interface IButton {
  active: boolean,
  isLeft?: boolean,
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: ${Styles.colors.white};
  cursor: pointer;
`;
const Button = styled.div<IButton>`
  width: 50%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;
  font-size: 12px;
  line-height: 20px;
  letter-spacing: 0.5px;
  text-transform: uppercase;
  
  ${props => props.isLeft ?
    'border-radius: 5px 0 0 5px;' : 'border-radius: 0 5px 5px 0;'
  }

  ${props => props.active ? `
    background-color: ${Styles.colors.blue};
    color: ${Styles.colors.white};
  ` : `
    background-color: ${Styles.colors.white};
    border: 1px solid #DFE5EC;
    color: ${Styles.colors.black};
  ` }
`;

const Tabs: React.FC<ITabs> = ({ activeTab, setActiveTab }) => {
  const setActiveCheap = () => setActiveTab('cheap'),
    setActiveFast = () => setActiveTab('fast'),
    isCheap = activeTab === 'cheap';
  return (
    <Wrapper>
      <Button isLeft active={isCheap} onClick={setActiveCheap}>самый дешевый</Button>
      <Button active={!isCheap} onClick={setActiveFast}>самый быстрый</Button>
    </Wrapper>
  );
};

export default Tabs;
