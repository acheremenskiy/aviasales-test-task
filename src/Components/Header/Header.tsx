import React from 'react';
import styled from 'styled-components';
import Logo from './Logo.png';

const LogoImage = styled.img`
  display: block;
  margin: auto;
  padding: 50px 0;
`;

const Header: React.FC = () => {
  return (
    <LogoImage src={Logo} alt="site logo"/>
  );
};

export default Header;
