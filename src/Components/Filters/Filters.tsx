import React from 'react';
import styled from 'styled-components';
import Styles from '../../styles';

interface IFilters {
  stops: number[],
  setStops: (stops: number[]) => void,
  setAllStops: () => void,
  setNullStops: () => void,
}

export const STOPS_LIST = ['Все', 'Без пересадок', '1 пересадка', '2 пересадки','3 пересадки'];

const Wrapper = styled.div`
  grid-area: filters;
  background: ${Styles.colors.white};
  box-shadow: ${Styles.border.boxShadow};
  border-radius: ${Styles.border.borderRadius};
  padding: 20px 0;
`;
const Title = styled.p`
  padding: 0 0 10px 20px;
  font-weight: 600;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.5px;
  color: ${Styles.colors.black};
  text-transform: uppercase;
`;
const CheckboxWrapper = styled.div`
  cursor: pointer;
`;
const Checkbox = styled.div`
  display: flex;
  padding: 10px 0 10px 20px;
  
  :hover {
    background-color: ${Styles.colors.hoverBackground};
  }
`;
const Input = styled.input`
  position: relative;
  width: 20px;
  height: 20px;
  margin: 0 10px 0 0;
  border: 1px solid #9ABBCE;
  box-sizing: border-box;
  border-radius: 2px;
  cursor: pointer;
  -webkit-appearance: none;
  outline: none;
  
  :after {
    content: none;
    position: absolute;
    top: 3px;
    left: 4px;
    display: inline-block;
    width: 9px;
    height: 6px;
    border-left: 2px solid ${Styles.colors.blue};
    border-bottom: 2px solid ${Styles.colors.blue};
    transform: rotate(-45deg);
  }
  
  &:checked[type="checkbox"] {
    border: 1px solid ${Styles.colors.blue};
    :after {
      content: '';
    }
  }
`;
const Label = styled.label`
  font-size: 13px;
  line-height: 20px;
  color: ${Styles.colors.black};
  cursor: pointer;
`;

const toggleStop = (stop: number, stopsArray: number[]) => {
  if (stopsArray.includes(stop)) {
    return stopsArray.filter(el => el !== stop);
  } else {
    return stopsArray.concat(stop);
  }
};

const Filters: React.FC<IFilters> = ({ stops, setStops, setAllStops, setNullStops }) => {
  return (
    <Wrapper>
      <Title>Количество пересадок</Title>
      <CheckboxWrapper>
        {
          STOPS_LIST.map((f, index) => {
            const stopsHandler = (e: any) => {
              // don't fire onClick on input
              if (e.target !== e.currentTarget && e.target.hasAttribute('checked')) return;
              return index ?
                setStops(toggleStop(index - 1, stops)) :
                stops.length === 4 ?
                  setNullStops() : setAllStops();
            };
            return (
            <Checkbox
              key={index}
              onClick={(e: any) => stopsHandler(e)}
            >
              <Input
                type="checkbox"
                name={`checkbox_${index}`}
                checked={index ? stops.includes(index - 1) : stops.length === 4}
                onChange={(e: any) => stopsHandler(e)}
              />
              <Label htmlFor={`checkbox_${index}`}>{f}</Label>
            </Checkbox>
          )
          })
        }
      </CheckboxWrapper>
    </Wrapper>
  );
};

export default Filters;
