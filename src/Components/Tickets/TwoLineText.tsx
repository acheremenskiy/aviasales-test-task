import React from 'react';
import styled from 'styled-components';
import Styles from '../../styles';

interface ITwoLines {
  title: string,
  description: string,
}

const Title = styled.p`
  font-size: 12px;
  font-weight: 600;
  line-height: 18px;
  letter-spacing: 0.5px;
  color: ${Styles.colors.gray};
  text-transform: uppercase;
`;
const Description = styled.p`
  font-size: 14px;
  font-weight: 600;
  line-height: 21px;
  color: ${Styles.colors.black};
`;

const TwoLinesText: React.FC<ITwoLines> = (props) => {
  return (
    <div>
      <Title>{props.title}</Title>
      <Description>{props.description}</Description>
    </div>
  );
};

export default TwoLinesText;
