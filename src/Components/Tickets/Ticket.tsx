import React from 'react';
import styled from 'styled-components';
import TwoLinesText from './TwoLineText';
import Styles from '../../styles';
import { ITicket } from '../../API/api';
import { STOPS_LIST } from '../Filters/Filters';

const options = {
  hour: 'numeric',
  minute: 'numeric',
};

const Wrapper = styled.div`
  grid-column: 2;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: 1.3fr 1fr 1fr;
  grid-template-areas: "price price logo";
  grid-column-gap: 20px;
  grid-row-gap: 10px;
  align-items: center;
  justify-content: flex-start;
  padding: 20px;
  background-color: ${Styles.colors.white};
  box-shadow: ${Styles.border.boxShadow};
  border-radius: ${Styles.border.borderRadius};
`;
const Price = styled.p`
  grid-area: price;
  font-size: 24px;
  font-weight: 600;
  color: ${Styles.colors.blue};
`;

const Ticket: React.FC<ITicket> = (props) => {
  return (
    <Wrapper>
      <Price>{(props.price).toLocaleString('ru')} P</Price>
      <img src={`//pics.avs.io/99/36/${props.carrier}.png`} alt="Airline Logo"/>
        {
          props.segments.map((el, i) => {
            const depatureDate = new Date(el.date),
              arrivalDate = new Date(el.date);
            arrivalDate.setMinutes(arrivalDate.getMinutes() + el.duration);
            const firstTime = depatureDate.toLocaleString('ru', options),
              secondTime = arrivalDate.toLocaleString('ru', options);
            return (
              <React.Fragment key={i}>
                <TwoLinesText
                  title={el.origin + ' - ' + el.destination}
                  description={firstTime + ' - ' + secondTime}
                />
                <TwoLinesText
                  title='в пути'
                  description={Math.floor(el.duration / 60) + 'ч ' + el.duration % 60 + 'м'}
                />
                <TwoLinesText
                  title={STOPS_LIST[el.stops.length + 1]}
                  description={el.stops.join(', ')}
                />
              </React.Fragment>
            )
          })
        }
    </Wrapper>
  );
};

export default Ticket;
