const Styles = {
  border: {
    borderRadius: '5px',
    boxShadow: '0px 2px 8px rgba(0, 0, 0, 0.1)',
  },
  colors: {
    backgroundColor: '#F3F7FA',
    hoverBackground: '#F1FCFF',
    black: '#4A4A4A',
    gray: '#A0B0B9',
    blue: '#2196F3',
    white: '#FFFFFF',
  }
};

export default Styles;