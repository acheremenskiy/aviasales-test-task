import React, { useState, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import Styles from './styles';
import Header from './Components/Header/Header';
import Filters from './Components/Filters/Filters';
import Tabs from './Components/Tabs/Tabs';
import Ticket from './Components/Tickets/Ticket';
import { getSearchId, getTickets, ITicket } from './API/api';

const Wrapper = styled.div`
  min-height: 100vh;
  background-color: ${Styles.colors.backgroundColor};
  font-family: 'Open Sans', sans-serif;
`;
const Body = styled.div`
  display: grid;
  grid-template-columns: 232px 502px;
  grid-template-rows: 50px repeat(auto-fill, 184px);
  grid-template-areas: "filters tabs" "filters tickets";
  grid-gap: 20px;
  justify-content: center;
  padding-bottom: 20px;
`;

const sortTickets = (tickets: ITicket[], activeTab: string, stops: number[]): ITicket[] => {
  let sortedArr;

  if (stops.length) {
    if (stops.length < 4) {
      sortedArr = tickets.filter(el => stops.includes(el.segments[0].stops.length) && stops.includes(el.segments[1].stops.length));
    } else sortedArr = tickets;
  } else return [];

  if (activeTab === 'cheap') {
    sortedArr = sortedArr.sort((a, b) => a.price - b.price);
  } else {
    sortedArr = sortedArr.sort((a, b) => (a.segments[0].duration + a.segments[1].duration) -
      (b.segments[0].duration + b.segments[1].duration)
    );
  }

  return sortedArr.splice(0,10); // 10 element is enough
};

// TODO add loader OR BETTER new experimental Concurrent Mode

const App: React.FC = () => {
  const [stops, setStops] = useState<number[]>([0, 1, 2, 3]);
  const setNullStops = () => setStops([]);
  const setAllStops = () => setStops([0, 1, 2, 3]);
  const [activeTab, setActiveTab] = useState<string>('cheap');
  const [allTickets, setAllTickets] = useState<ITicket[]>([]);

  useEffect(() => {
    (async () => {
      const { searchId }: ({ searchId: string }) = await getSearchId();
      const responseTickets = await getTickets(searchId);
      setAllTickets(responseTickets);
    })();
  }, []);

  // it's working ?????????????
  const memoTickets = useMemo(() => sortTickets(allTickets, activeTab, stops), [allTickets, activeTab, stops]);

  return (
    <Wrapper>
      <Header/>
      <Body>
        <Filters
          stops={stops}
          setStops={setStops}
          setAllStops={setAllStops}
          setNullStops={setNullStops}
        />
        <Tabs
          activeTab={activeTab}
          setActiveTab={setActiveTab}
        />
        {
          memoTickets.map((el: ITicket, i) => <Ticket key={i} {...el}/>)
        }
      </Body>
    </Wrapper>
  );
};

export default App;
